= Filmotec
:showtitle:

Niveau : Deuxième année de BTS SIO SLAM

Membre du groupe : FORTRIE Louis et DELENIN Mathéo

== Objectif du projet

Concevoir une application web cliente développée en *TS* avec *Angular*, s'appuyant sur l'*API TMDB* comme source de données. L'utilisateur pourras rechercher un film, l'ajouter à la liste de ses favoris, le noter et le commenter.

== Présentation de l'API TMDB

L'API TMDB est une API dedié aux films, séries et acteurs. On retrouve dans cette API de nombreuse informations comme le casting, le synopsis ou les affiches de film/série. Nous nous sommes focalisés sur ce projet sur les informations des films et leurs castings. Par le biais de requête (que nous vous présenterons plus tard), nous pouvons rechercher un film, et ceux par plusieurs moyens, comme leur nom ou leur id.


== Présentation des composant

Ce projet s'articule autour de 3 composants :

* Le composant `Home` responsable de la recherche d'un film grâce a son nom, et de l'affichage de la liste des résultat
* Le composant `Film` responsable de l'affichage des informations d'un film, comme son casting, sa direction, sa date de sortie... C'est aussi par le biais de ce composant que l'utilisateur peut ajouter le film a ses favoris, puis le commenter et le noter.
* La composant `Liste` responsable l'affichage de la liste des favoris de l'utilisateur


== Présentation du code
=== Analyse du code

Lorsque l'utilisateur arrive sur le site, il est accueilli par un formulaire, permettant la saisie du titre du film voulu

image::images/home.JPG[]

Voici le code de ce formulaire, présent dans le fichier `home.component.html` :

[, html]
----
<h3 class="title is-3">Rechercher un titre de Film</h3>


        <form [formGroup]="identForm" (ngSubmit)="recherche()">
          <div class="field">   <1>
            <label class="label">titre</label>
            <div class="control">
              <input class="input is-success" formControlName="title" type="text"
                placeholder="Star Wars ou tout autre film pour faire plaisir a un camarade dont je tairais le prénom..."
                value=""> <2>
            </div>
          </div>
          <div class="field is-grouped">
            <div class="control">
              <button class="button is-link">Soumettre</button>
            </div>
          </div>
        </form>
----

<1> Lorsque l'utilisateur clique sur le bouton soumettre, on appelle la méthode `recherche()` du fichier `home.component.ts`

<2> On récupère donc la valeur `title` dans le `input` grâce au `FormControl`

Une fois dans le fichier `home.component.ts`, voici le code de notre fonction `recherche()`

[, typescript]
----
 recherche() {
    this.isSubmitted = true;
    console.log('Titre saisie : ', this.identForm.value);
    this.FilmService.sendGetRequest(this.identForm.value.title, this.numPage).subscribe((data: Object) => {   <1>
      this.numPage = data['page'];
      this.nbPages = data['total_pages']
      this.FilmService.addInfos(data['results']);  <2>
      this.lesFilms = this.FilmService.getAllInfos();  <3>
      console.log(data['results']);
    });
  }
----

<1> On appelle la méthode `sendGetRequest()` en lui passant la valeur récupérés dans notre formulaire (avec le `FormControl`), cette fonction envoie une requête à l'API comportant le titre du film saisie par l'utilisateur

<2> On appele notre méthode `addInfos()` présente dans le fichier `film.service.ts`, en lui passant l'objet `result`, renvoyé par la requête et symbolisé par l'objet `data`

<3> On récupère dans le tableau `lesFilms` tous les films présent dans l'objet `result` grâce à la méthode `getAllInfos()` présent dans le fichier `film.service.ts`

Nous allons maintenant décrire ces diverses méthodes avec plus de détails

[, typescript]
----
    public sendGetRequest(title: string, numPage: number) {
    return this.http.get(environment.ApiUrl + environment.API_TOKEN + '&language=fr&query=' + title + '&page=' + numPage);
  } <1>

----

<1> Cette méthode envoie à l'API une requête permettant de chercher un film grâce à son nom et nous renvoie le resultat sous forme d'objet. Elle est composé de diverse variable présente dans le fichier `environment.ts` dont voici le détail :

* `environment.ApiURL` correspond à la l'url permettant la recherche d'un film par son nom : 'https://api.themoviedb.org/3/search/movie
* `title` correspond à la variable passée à la méthode correspondant au titre saisie par l'utilisateur




image::images/result.JPG[]




Une fois que le film désiré par l'utilisateur affiché, un clic permet de l'envoyer vers la page contenant les informations de l'oeuvre
Lors du chargement du composant `Film`, la fonction `ngOnInit()` s'éxécute

[, typescript]
----
ngOnInit(): void {
      this.activatedRoute.params.subscribe(params => {
        const idURL: number= params['id'];     <1>
        this.FilmService.sendGetInformationRequest(idURL).subscribe((data: Object) => {          <2>
          this.FilmService.addInfos(Object(data));
          this.lesFilms = this.FilmService.getAllInfos();  <3>
        });
        this.FilmService.sendGetCreditsRequest(idURL).subscribe((data: Object) => {      <4>
          this.FilmService.addInfos(data['crew']);    <5>
          this.crewCredits = this.FilmService.getAllInfos();
          this.FilmService.addInfos(data['cast']);
          this.castCredits = this.FilmService.getAllInfos();
          for( let i = 0; i< this.crewCredits.length; i++){
            if(this.crewCredits[i].job == 'Director'){
              this.directorArray.push([this.crewCredits[i].name] );  <6>

            }
          }
          for( let i = 0; i< 5 ; i++){
            this.actorsArray.push(this.castCredits[i].name); <7>
        }
          this.lesFavoris = this.FilmService.getAllFavoris();  <8>
        });
    });
  }
----

<1> Grâce au module `ActivatedRoute`, on récupère l'id que l'on a fait passée lors du clic dans le module précédant

<2> On fait appelle à la requête `sendGetInformationRequest()` , qui nous permet de récupérer toutes les informations d'un film, grâce a son id. Elle est composé d'un lien (`apiFullMovieURL`), de l'id du film ainsi que de notre clef d'API (`API_TOKEN`), nécessaire à tous développeurs voulant utilisé cette API.
Voici à quoi ressemble cette requête :


    public sendGetInformationRequest(idFilm: number) {
        return this.http.get(environment.apiFullMovieURL + idFilm + environment.API_TOKEN + '&language=fr')
  }


<3> Comme dans le composant `Home`, on récupère le résultat de la requête et on le place dans un tableau, grâce aux méthodes `addInfos()`et `getAllInfos()`

<4> On fait appelle à la méthode `sendGetCreditRequest()`, qui nous permet de recupérer les informations du casting du film. Elle est composé d'un lien (`apiFullMovieURL`), de l'id du film ainsi que de notre clef d'API (`API_TOKEN`), mais aussi de la variable `apiCreditURL`

    public sendGetCreditsRequest(idFilm: number) {
    return this.http.get(environment.apiFullMovieURL + idFilm + environment.apiCreditsURL + environment.API_TOKEN + environment.apiLanguageFr)

  }



<5> On utilise ensuite les méthodes `addInfos` et `getAllInfos` pour récuperer le casting et l'équipe technique dans 2 tableaux respectifs `crewCredits` et `castCredits`

<6> On crée une boucle pour récupérer tous les membres du crew ayant comme profession 'director'

<7> On utilise ensuite une boucle `for` pour ajouter dans un tableau les 5 premiers acteur du tableau contenant tous les acteurs

<8> Grâce à la fonction `getAllFavoris()`, on récupère la liste de tous les favoris, ce qui nous serviras plus tard

Une fois le résultat affiché, l'utilisateur peut ajouter un film à ses favoris grâce à un bouton

[, html]
----
div class="field is-grouped">
                <div *ngIf="showFavoriteButton==true">
                  <button class="button is-success" (click)="onClicFavoris(lesFilms)">
                    <span class="icon is-small">
                      <i class="far fa-check-square"></i>
                    </span>
                    <span>Ajouter aux favoris</span>
                  </button>
                </div>
----


Lors d'un clic sur ce bouton, il appelle la fonction `OnClicFavoris`

[, typescript]
----
onClicFavoris(Film: Film) {

    this.activatedRoute.params.subscribe(params => {
      const idURL: number = params['id'];     <1>

      this.FilmService.sendGetInformationRequest(idURL).subscribe((data: Object) => {
        this.lesFavoris = this.FilmService.getAllFavoris();        <2>
        let bin = true;                                            <3>
        for (let i = 0; i < this.lesFavoris.length; i++) {         <4>
          if (this.lesFavoris[i].id == Film.id) {
            bin = false;
          }
        }
        if (bin == true) {
          this.FilmService.addFavoris(Object(data));                <5>
        }
      });
    });
    this.showFavoriteButton = false;
  }
----

<1> On récupère l'id du film que l'on assigne à la variable `number`

<2> On appelle la fonction `getAllFavoris`, qui renvoie un tableau contenant la liste des films favoris (dont nous allons décrire le fonctionnement plus tard)

<3> On initialise une variable `bin`, qui changeras de valeur selon l'avancement de la fonction

<4> On crée une boucle parcourant tout le tableau contenant les favoris et on compare notre id aux différend id des objets Film présent dans le tableau

<5> Si la valeur de `bin` n'a pas changé, c'est que le Film n'est pas encore ajouté aux favoris, on appelle donc la fonction `addFavoris()`, qui ajoute le Film a la liste des Favoris

[, typescript]
----
addFavoris(Film: Film) {
    this.favoris = localStorage.getItem('FilmL') ? JSON.parse(localStorage.getItem('FilmL')) : []; <1>
    let bin = true;
    for (let i = 0; i < this.favoris.length; i++) {
      if (this.favoris[i].id == Film.id) {
        bin = false;
      }
    }
    if (bin == true) {
      this.favoris.push(Film);  <2>
      localStorage.setItem('FilmL', JSON.stringify(this.favoris));  <3>
    }
    this.favoris = this.getAllFavoris();   <4>
    location.reload() <5>
  }
----

<1> On initialise une variable `favorism` qui est la liste des film stocké dans le localstorage

<2> On inscrit dans la liste `favoris` le Film que l'on souhaite ajoutés au favoris

<3> On inscrit dans le localstorage la liste des films favoris, comportant le film que l'on vient d'ajouter

<4> On appelle la fonction `getAllFavoris()` pour récupérer cette nouvelle liste

<5> On force un rechargement de la page pour afficher les nouvelles features associés aux films favoris

image::images/fav.JPG[*]*

image::images/film.JPG[]

Il ne nous reste plus qu'a afficher la liste des favoris, que l'on récupère grâce à la fonction `getAllFavoris()`

[, typescript]
----
getAllFavoris() {
    this.favoris = JSON.parse(localStorage.getItem('FilmL') || '[]');                  <1>

    return this.favoris    <2>
  }
----

<1> On convertie la liste des films que l'on récupère dans le localstorage en objet JSON, que l'on associe à une variable `favoris`

<2> On retourne la variable `favoris`

Pour afficher la liste des favoris, on utilise notre composant `Liste`, et plus particulièrement le fichier `liste.component.html`

[, html]
----
<ul *ngFor="let fav of lesFavoris ; let i =index">  <1>
    <section class="columns is-mobile is-multiline is-centered mt-2">

        <div class="column box is-6 has-background-light mt-5">
          <a [routerLink]="['/film/', fav.id ]" class=" has-text-dark">    <2>
      <article class="media">

        <figure class="media-left">
          <p>
            <li><img src="https://image.tmdb.org/t/p/w500{{ fav.poster_path }}" width="150"></li>   <3>
          </p>

        </figure>
        <div class="media-content">
          <div class="content">
            <p>
            <strong><li>{{ fav.title }}</li></strong>
              <br>
              <li>Date de sortie : {{ fav.release_date }}</li>
              <li>Synopsis : {{ fav.overview }} </li>
            </p>
          </div>
----

<1> On initialise une liste avec la balise <ul> , que l'on fait boucler grâce à l'instruction *ngFor, et qui parcourt la liste des films favoris

<2> Grâce au module `RouterModule`, si l'utilisateur clique sur un des films, ils seras renvoyés vers la page du film grâce à l'id inclus dans le lien

<3> Pour afficher une informations, il suffit d'indiquer la clef ainsi que la valeur à afficher, séparés par un point.


image::images/favoris.JPG[]

== Conclusion

Pour conclure, notre projet permet la recherche de films, leur ajout à une liste, d'indiquer un commentaire et une note ainsi que le retirer de ses favoris
Ce projet nous as amené à découvrir de nouvelles notions comme les API, les observables, ainsi que les requête HTTP.

Malgré le manque de temps et une période de développement compliqué, nous sommes content de notre projet et nous l'avons trouvé enrichissant, nous amenant à découvrir de nouvelles notions et de consolider celles deja étudiés.
De plus, ce projet nous a permis d'aborder différement les projets, en mettant l'accent sur l'analyse et les diagrammes.


lien du dépot GITLAB : https://gitlab.com/Grunt471/filmotec

Réalisé le 30/03/2021
